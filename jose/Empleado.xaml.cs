﻿using jose.Clases;
using jose.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace jose
{
    /// <summary>
    /// Lógica de interacción para Empleado.xaml
    /// </summary>
    public partial class Empleado : Window
    {
        RepositorioEmpleados repo;
        bool nuevo;
        public Empleado()
        {
            InitializeComponent();
            repo = new RepositorioEmpleados();
            ActualizarTabla();
            habilitado(false);
            HabilitarBotones(true);
        }

        private void ActualizarTabla()
        {
            dtgEmpleado.ItemsSource = null;
            dtgEmpleado.ItemsSource = repo.Leer();

        }

        private void HabilitarBotones(bool v)
        {
            btnCancelar.IsEnabled = !v;
            btnEditar.IsEnabled = v;
            btnGuardar.IsEnabled = !v;
            btnNuevo.IsEnabled = v;
            btnEliminar.IsEnabled = v;
        }

        private void habilitado(bool habilitado)
        {
            txtCorreo.Clear();
            txtDireccion.Clear();
            txtNombre.Clear();
            txtSueldo.Clear();
            txtTelefono.Clear();

            txtTelefono.IsEnabled = habilitado;
            txtSueldo.IsEnabled = habilitado;
            txtNombre.IsEnabled = habilitado;
            txtDireccion.IsEnabled = habilitado;
            txtCorreo.IsEnabled = habilitado;

        }


        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            MenuPrincipal menuprincipal = new MenuPrincipal();
            menuprincipal.Show();
            this.Close();

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            habilitado(true);
            HabilitarBotones(false);
            nuevo = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            habilitado(false);
            HabilitarBotones(true);
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun empleado", "Empleados", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgEmpleado.SelectedItem != null)
                {
                    EmpleadoJ a = dtgEmpleado.SelectedItem as EmpleadoJ;
                    habilitado(true);
                    txtCorreo.Text = a.Correo;
                    txtDireccion.Text = a.Direccion;
                    txtNombre.Text = a.Nombre;
                    txtSueldo.Text = a.Sueldo;
                    txtTelefono.Text = a.Telefono;
                    HabilitarBotones(false);
                    nuevo = false;
                }
                else
                {
                    MessageBox.Show("Debe seleccionar en la tabla a quien desea editar", "Empleados", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        
        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun empleado", "Empleados", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgEmpleado.SelectedItem != null)
                {
                    EmpleadoJ a = dtgEmpleado.SelectedItem as EmpleadoJ;
                    if (MessageBox.Show("Esta seguro de dar de baja a " + a.Nombre, "Empleado", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repo.Eliminar(a))
                        {
                            MessageBox.Show("Empleado dado de baja", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("No se ha podido eliminar tu empleado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar a un empleado de la tabla", "Empleado", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void GuardarElemtos() {
            EmpleadoJ a = new EmpleadoJ()
            {
                Nombre = txtNombre.Text,
                Telefono = txtTelefono.Text,
                Correo = txtCorreo.Text,
                Direccion = txtDireccion.Text,
                Sueldo = txtSueldo.Text
            };
            if (repo.Agregar(a))
            {
                MessageBox.Show("Dato guardado sadisfactoriamnete", "Empleados", MessageBoxButton.OK, MessageBoxImage.Information);
                ActualizarTabla();
                HabilitarBotones(true);
                habilitado(false);
            }
            else
            {
                MessageBox.Show("No se guardo correctamente el empleado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GuardarEditado() {
            EmpleadoJ original = dtgEmpleado.SelectedItem as EmpleadoJ;
            EmpleadoJ a = new EmpleadoJ();

            a.Correo = txtCorreo.Text;
            a.Direccion = txtDireccion.Text;
            a.Nombre = txtNombre.Text;
            a.Sueldo = txtSueldo.Text;
            a.Telefono = txtTelefono.Text;
            if (repo.Modificar(original, a))
            {
                HabilitarBotones(true);
                habilitado(false);
                ActualizarTabla();
                MessageBox.Show("Empleado editado correctamente", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("No se edito correctamnete el empleado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (nuevo)
            {
                GuardarElemtos();
            }
            else {
                GuardarEditado();
            }
        }
           
    }
}
