﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace jose
{
    /// <summary>
    /// Interaction logic for CategoriaFarmacia.xaml
    /// </summary>
    public partial class CategoriaFarmacia : Window
    {
        RepositorioCategoria repo;
        bool nuevo;
        public categoria()
        public CategoriaFarmacia()
        {
            InitializeComponent();
            repo = new RepositorioCategoria();
            ActualizarTabla();
            habilitado(false);
            HabilitarBotones(true);
        }
        private void ActualizarTabla()
        {
            dtgCategoria.ItemsSource = null;
            dtgCategoria.ItemsSource = repo.Leer();

        }
        private void HabilitarBotones(bool v)
        {
            btnCancelar.IsEnabled = !v;
            btnEditar.IsEnabled = v;
            btnGuardar.IsEnabled = !v;
            btnNuevo.IsEnabled = v;
            btnEliminar.IsEnabled = v;
        }
        private void habilitado(bool habilitado)
        {
            txtCategoria.Clear();

            txtCategoria.IsEnabled = habilitado;


        }
        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            MenuPrincipal menuprincipal = new MenuPrincipal();
            menuprincipal.Show();
            this.Close();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            habilitado(true);
            HabilitarBotones(false);
            nuevo = true;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun empleado", "Empleados", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgCategoria.SelectedItem != null)
                {
                    EmpleadoJ a = dtgEmpleado.SelectedItem as EmpleadoJ;
                    habilitado(true);
                    txtCorreo.Text = a.Correo;
                    txtDireccion.Text = a.Direccion;
                    txtNombre.Text = a.Nombre;
                    txtSueldo.Text = a.Sueldo;
                    txtTelefono.Text = a.Telefono;
                    HabilitarBotones(false);
                    nuevo = false;
                }
                else
                {
                    MessageBox.Show("Debe seleccionar en la tabla a quien desea editar", "Empleados", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            habilitado(false);
            HabilitarBotones(true);
        }
    }
}
