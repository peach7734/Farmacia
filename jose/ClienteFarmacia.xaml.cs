﻿using jose.Clases;
using jose.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace jose
{
    /// <summary>
    /// Interaction logic for ClienteFarmacia.xaml
    /// </summary>
    public partial class ClienteFarmacia : Window
    {
        RepositorioClientes repo;
        bool nuevo;

        public ClienteFarmacia()
        {
            InitializeComponent();
            repo = new RepositorioClientes();
            ActualizarTabla();
            habilitado(false);
            HabilitarBotones(true);
        }



        private void ActualizarTabla()
        {
            dtgCliente.ItemsSource = null;
            dtgCliente.ItemsSource = repo.Leer();

        }

        private void HabilitarBotones(bool v)
        {
            btnCancelar.IsEnabled = !v;
            btnEditar.IsEnabled = v;
            btnGuardar.IsEnabled = !v;
            btnNuevo.IsEnabled = v;
            btnEliminar.IsEnabled = v;
        }

        private void habilitado(bool habilitado)
        {
            txtCorreo.Clear();
            txtDireccion.Clear();
            txtNombre.Clear();
            txtTelefono.Clear();
            txtRFC.Clear();

            txtTelefono.IsEnabled = habilitado;
            txtNombre.IsEnabled = habilitado;
            txtDireccion.IsEnabled = habilitado;
            txtCorreo.IsEnabled = habilitado;
            txtRFC.IsEnabled = habilitado;

        }

        private void GuardarEditado()
        {
            clientesjose original = dtgCliente.SelectedItem as clientesjose;
            clientesjose a = new clientesjose();
            
            a.Correo = txtCorreo.Text;
            a.Direccion = txtDireccion.Text;
            a.Nombre = txtNombre.Text;
            a.Telefono = txtTelefono.Text;
            a.RFC = txtRFC.Text;
            if (repo.Modificar(original, a))
            {
                HabilitarBotones(true);
                habilitado(false);
                ActualizarTabla();
                MessageBox.Show("Cliente editado correctamente", "Cliente", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("No se edito correctamnete el Cliente", "Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GuardarElemtos()
        {
            clientesjose a = new clientesjose()
            {
                Nombre = txtNombre.Text,
                Telefono = txtTelefono.Text,
                Correo = txtCorreo.Text,
                Direccion = txtDireccion.Text,
                RFC = txtRFC.Text
            };
            if (repo.Agregar(a))
            {
                MessageBox.Show("Dato guardado sadisfactoriamnete", "Cliente", MessageBoxButton.OK, MessageBoxImage.Information);
                ActualizarTabla();
                HabilitarBotones(true);
                habilitado(false);
            }
            else
            {
                MessageBox.Show("No se guardo correctamente el Cliente", "Clientes", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (nuevo)
            {
                GuardarElemtos();
            }
            else
            {
                GuardarEditado();
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            habilitado(false);
            HabilitarBotones(true);

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun Cliente", "Clientes", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgCliente.SelectedItem != null)
                {
                    clientesjose a = dtgCliente.SelectedItem as clientesjose;
                    if (MessageBox.Show("Esta seguro de dar de baja a " + a.Nombre, "Cliente", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repo.Eliminar(a))
                        {
                            MessageBox.Show("Cliente dado de baja", "Cliente", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("No se ha podido eliminar tu Cliente", "Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar a un Cliente de la tabla", "Cliente", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun Cliente", "Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgCliente.SelectedItem != null)
                {
                    clientesjose a = dtgCliente.SelectedItem as clientesjose;
                    habilitado(true);
                    txtCorreo.Text = a.Correo;
                    txtDireccion.Text = a.Direccion;
                    txtNombre.Text = a.Nombre;
                    txtRFC.Text = a.RFC;
                    txtTelefono.Text = a.Telefono;
                    HabilitarBotones(false);
                    nuevo = false;
                }
                else
                {
                    MessageBox.Show("Debe seleccionar en la tabla a quien desea editar", "Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            habilitado(true);
            HabilitarBotones(false);
            nuevo = true;
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            MenuPrincipal menuprincipal = new MenuPrincipal();
            menuprincipal.Show();
            this.Close();
        }
    }
}
