﻿using jose.Archivo;
using jose.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jose.Repo
{
    public class RepositorioEmpleados
    {

        Run accionesArchivo;
        List<EmpleadoJ> Empleados;
        public RepositorioEmpleados()
        {
            accionesArchivo = new Run("Empleados.poo");
            Empleados = new List<EmpleadoJ>();
        }

        public bool Agregar(EmpleadoJ emple)
        {
            Empleados.Add(emple);
            bool resultado = ActualizarArchivo();
            Empleados = Leer();
            return resultado;
        }

        public bool Eliminar(EmpleadoJ emple)
        {
            EmpleadoJ tiempo = new EmpleadoJ();
            foreach (var Buscador in Empleados)
            {
                if (Buscador.Telefono == emple.Telefono)
                {
                    tiempo = Buscador;
                }
            }
            Empleados.Remove(tiempo);
            bool resultado = ActualizarArchivo();
            Empleados = Leer();
            return resultado;
        }

        public bool Modificar(EmpleadoJ original, EmpleadoJ modificado)
        {
            EmpleadoJ t = new EmpleadoJ();
            foreach (var buscador in Empleados)
            {
                if (original.Telefono == buscador.Telefono)
                {
                    t = buscador;
                }
            }
           
            t.Correo = modificado.Correo;
            t.Direccion = modificado.Direccion;
          
            t.Nombre = modificado.Nombre;
            t.Sueldo = modificado.Sueldo;
            t.Telefono = modificado.Telefono;
            bool resultado = ActualizarArchivo();
            Empleados = Leer();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string elemntos = "";
            foreach (EmpleadoJ item in Empleados)
            {
                elemntos += string.Format("{0}|{1}|{2}|{3}|{4}\n", item.Nombre, item.Direccion, item.Telefono, item.Correo, item.Sueldo);
            }
            return accionesArchivo.GuardarDatos(elemntos);
        }
        public List<EmpleadoJ > Leer()
        {
            string elementos = accionesArchivo.Leer();
            if (elementos != null)
            {
                List<EmpleadoJ> emp = new List<EmpleadoJ>();
                string[] fila = elementos.Split('\n');
                for (int i = 0; i < fila.Length - 1; i++)
                {
                    string[] espacio = fila[i].Split('|');
                    EmpleadoJ a = new EmpleadoJ();
                    a.Nombre = espacio[0];
                    a.Direccion = espacio[1];
                    a.Telefono = espacio[2];
                    a.Correo = espacio[3];
                    a.Sueldo = espacio[4];
 
                    emp.Add(a);
                }
                Empleados = emp;
                return emp;
            }
            else
            {
                return null;
            }
        }
    
}
}
