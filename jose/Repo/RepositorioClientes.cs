﻿using jose.Archivo;
using jose.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Documents;

namespace jose
{
    public class RepositorioClientes
    {
        Run accionesArchivo;
        List<Clases.clientesjose> Cliente;
        public RepositorioClientes()
        {
            accionesArchivo = new Run("Clietes.poo");
            Cliente = new List<Clases.clientesjose>();
        }

        public bool Agregar(Clases.clientesjose emple)
        {
            Cliente.Add(emple);
            bool resultado = ActualizarArchivo();
            Cliente = Leer();
            return resultado;
        }

        public bool Eliminar(Clases.clientesjose emple)
        {
            Clases.clientesjose tiempo = new Clases.clientesjose();
            foreach (var Buscador in Cliente)
            {
                if (Buscador.RFC == emple.RFC)
                {
                    tiempo = Buscador;
                }
            }
            Cliente.Remove(tiempo);
            bool resultado = ActualizarArchivo();
            Cliente = Leer();
            return resultado;
        }

        public bool Modificar(Clases.clientesjose original, Clases.clientesjose modificado)
        {
            Clases.clientesjose t = new Clases.clientesjose();
            foreach (var buscador in Cliente)
            {
                if (original.Telefono == buscador.Telefono)
                {
                    t = buscador;
                }
            }

            t.Correo = modificado.Correo;
            t.Direccion = modificado.Direccion;
            t.Nombre = modificado.Nombre;
            t.RFC = modificado.RFC;
            t.Telefono = modificado.Telefono;
            bool resultado = ActualizarArchivo();
            Cliente = Leer();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string elemntos = "";
            foreach (Clases.clientesjose item in Cliente)
            {
                elemntos += string.Format("{0}|{1}|{2}|{3}|{4}\n", item.Nombre, item.Direccion, item.Telefono, item.Correo, item.RFC);
            }
            return accionesArchivo.GuardarDatos(elemntos);
        }
        public List<Clases.clientesjose> Leer()
        {
            string elementos = accionesArchivo.Leer();
            if (elementos != null)
            {
                List<Clases.clientesjose> emp = new List<Clases.clientesjose>();
                string[] fila = elementos.Split('\n');
                for (int i = 0; i < fila.Length - 1; i++)
                {
                    string[] espacio = fila[i].Split('|');
                    Clases.clientesjose a = new Clases.clientesjose();
                    a.Nombre = espacio[0];
                    a.Direccion = espacio[1];
                    a.Telefono = espacio[2];
                    a.Correo = espacio[3];
                    a.RFC = espacio[4];

                    emp.Add(a);
                }
                Cliente = emp;
                return emp;
            }
            else
            {
                return null;
            }
        }


    }
}
