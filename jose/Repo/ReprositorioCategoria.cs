﻿using jose.Archivo;
using jose.Clases;
using Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Repositoriosk
{
    public class RepositorioCategorias
    {
        Run accionesArchivo;
        List<FarmaciaCategoria> categorias;
        public RepositorioCategorias()
        {
            accionesArchivo = new Run("Categorias.poo");
            categorias = new List<FarmaciaCategoria>();
        }

        public bool Agregar(FarmaciaCategoria inv)
        {
            categorias.Add(inv);
            bool accion = ActualizarArchivo();
            categorias = Leer();
            return accion;
        }

        private bool ActualizarArchivo()
        {
            string elementos = "";
            foreach (FarmaciaCategoria item in categorias)
            {
                elementos += string.Format("{0}\n", item.Categoria);
            }
            return accionesArchivo.GuardarDatos(elementos);
        }

        public List<FarmaciaCategoria> Leer()
        {
            string elementos = accionesArchivo.Leer();
            if (elementos != null)
            {
                List<FarmaciaCategoria> inv = new List<FarmaciaCategoria>();
                string[] fila = elementos.Split('\n');
                for (int i = 0; i < fila.Length - 1; i++)
                {
                    string[] espacio = fila[i].Split('|');
                    FarmaciaCategoria a = new FarmaciaCategoria();
                    a.Categoria = (espacio[0]);


                    inv.Add(a);
                }
                categorias = inv;
                return inv;
            }
            else
            {
                return null;
            }
        }

        public bool Eliminar(FarmaciaCategoria cat)
        {
            FarmaciaCategoria categori = new FarmaciaCategoria();
            foreach (var Buscador in categorias)
            {
                if (Buscador.Categoria == cat.Categoria)
                {
                    categori = Buscador;
                }
            }
            categorias.Remove(categori);
            bool accion = ActualizarArchivo();
            categorias = Leer();
            return accion;
        }

        public bool Modificar(FarmaciaCategoria original, FarmaciaCategoria modificado)
        {
            FarmaciaCategoria t = new FarmaciaCategoria();
            foreach (var buscador in categorias)
            {
                if (original.Categoria == buscador.Categoria)
                {
                    t = buscador;
                }
            }
            t.Categoria = modificado.Categoria;

            bool resultado = ActualizarArchivo();
            categorias = Leer();
            return resultado;
        }
    }
}
