﻿using jose.Archivo;
using jose.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Documents;

namespace jose.Repo
{
    public class RepositorioProductos
    {
        Run accionesArchivo;
        List<Clases.Productos> productos;
        public RepositorioProductos()
        {
            accionesArchivo = new Run("Productos.poo");
            productos = new List<Clases.Productos>();
        }

        public bool Agregar(Clases.Productos emple)
        {
            productos.Add(emple);
            bool resultado = ActualizarArchivo();
            productos = Leer();
            return resultado;
        }

        public bool Eliminar(Clases.Productos emple)
        {
            Clases.Productos tiempo = new Clases.Productos();
            foreach (var Buscador in productos)
            {
                if (Buscador.nombre == emple.nombre)
                {
                    tiempo = Buscador;
                }
            }
            productos.Remove(tiempo);
            bool resultado = ActualizarArchivo();
            productos = Leer();
            return resultado;
        }

        public bool Modificar(Clases.Productos original, Clases.Productos modificado)
        {
            Clases.Productos t = new Clases.Productos();
            foreach (var buscador in productos)
            {
                if (original.nombre == buscador.nombre)
                {
                    t = buscador;
                }
            }

            t.categoria = modificado.categoria;
            t.descripcion = modificado.descripcion;
            t.nombre = modificado.nombre;
            t.precioCompra = modificado.precioCompra;
            t.precioVenta = modificado.precioVenta;
            t.presentacion = modificado.presentacion;
            bool resultado = ActualizarArchivo();
            productos = Leer();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string elemntos = "";
            foreach (Clases.Productos item in productos)
            {
                elemntos += string.Format("{0}|{1}|{2}|{3}|{4}|{5}\n", item.categoria, item.descripcion, item.nombre, item.precioCompra, item.precioVenta, item.presentacion);
            }
            return accionesArchivo.GuardarDatos(elemntos);
        }
        public List<Clases.Productos> Leer()
        {
            string elementos = accionesArchivo.Leer();
            if (elementos != null)
            {
                List<Clases.Productos> emp = new List<Clases.Productos>();
                string[] fila = elementos.Split('\n');
                for (int i = 0; i < fila.Length - 1; i++)
                {
                    string[] espacio = fila[i].Split('|');
                    Clases.Productos a = new Clases.Productos();
                    a.categoria = espacio[0];
                    a.descripcion = espacio[1];
                    a.nombre = espacio[2];
                    a.precioCompra = espacio[3];
                    a.precioVenta = espacio[4];
                    a.presentacion = espacio[5];

                    emp.Add(a);
                }
                productos = emp;
                return emp;
            }
            else
            {
                return null;
            }
        }
        
    
    }
}
