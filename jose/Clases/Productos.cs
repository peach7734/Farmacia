﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jose.Clases
{
   public class Productos
    {
        public string categoria { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string precioCompra { get; set; }
        public string precioVenta { get; set; }
        public string presentacion { get; set; }
    }
}
