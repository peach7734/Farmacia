﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using jose.Clases;
using jose.Repo;

namespace jose
{
    /// <summary>
    /// Interaction logic for Producto.xaml
    /// </summary>
    public partial class Producto : Window
    {
        RepositorioProductos repo;
        bool nuevo;
        public Producto()
       
        {
            InitializeComponent();
            repo = new RepositorioProductos();
            ActualizarTabla();
            habilitado(false);
            HabilitarBotones(true);
        }

        private void ActualizarTabla()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = repo.Leer();

        }

        private void HabilitarBotones(bool v)
        {
            btnCancelar.IsEnabled = !v;
            btnEditar.IsEnabled = v;
            btnGuardar.IsEnabled = !v;
            btnNuevo.IsEnabled = v;
            btnEliminar.IsEnabled = v;
        }
        private void habilitado(bool habilitado)
        {
            txtPrecioCompra.Clear();
            txtPrecioVenta.Clear();
            txtNombre.Clear();
            txtCategoria.Clear();
            txtDescripcion.Clear();
            txtPresentacion.Clear();

            txtPrecioCompra.IsEnabled = habilitado;
            txtPrecioVenta.IsEnabled = habilitado;
            txtNombre.IsEnabled = habilitado;
            txtCategoria.IsEnabled = habilitado;
            txtDescripcion.IsEnabled = habilitado;
            txtPresentacion.IsEnabled = habilitado;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            habilitado(true);
            HabilitarBotones(false);
            nuevo = true;
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (nuevo)
            {
                GuardarElementos();
            }
            else
            {
                GuardarEditado();
            }
        }

        private void GuardarEditado()
        {
            Clases.Productos original = dtgProductos.SelectedItem as Clases.Productos;
            Clases.Productos a = new Clases.Productos();

            a.categoria = txtCategoria.Text;
            a.descripcion = txtDescripcion.Text;
            a.nombre = txtNombre.Text;
            a.precioVenta = txtPrecioVenta.Text;
            a.precioCompra = txtPrecioCompra.Text;
            a.presentacion = txtPresentacion.Text;
            if (repo.Modificar(original, a))
            {
                HabilitarBotones(true);
                habilitado(false);
                ActualizarTabla();
                MessageBox.Show("Producto editado correctamente", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("No se edito correctamnete el Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void GuardarElementos()
        {
            Clases.Productos a = new Clases.Productos()
            {
                nombre = txtNombre.Text,
                categoria = txtCategoria.Text,
                descripcion = txtDescripcion.Text,
                precioCompra = txtPrecioCompra.Text,
                precioVenta = txtPrecioVenta.Text,
                presentacion = txtPresentacion.Text,

            };
            if (repo.Agregar(a))
            {
                MessageBox.Show("Dato guardado sadisfactoriamnete", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                ActualizarTabla();
                HabilitarBotones(true);
                habilitado(false);
            }
            else
            {
                MessageBox.Show("No se guardo correctamente el Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgProductos.SelectedItem != null)
                {
                    Clases.Productos a = dtgProductos.SelectedItem as Clases.Productos;
                    habilitado(true);
                    txtCategoria.Text = a.categoria;
                    txtNombre.Text = a.nombre;
                    txtDescripcion.Text = a.descripcion;
                    txtPrecioCompra.Text = a.precioCompra;
                    txtPrecioVenta.Text = a.precioVenta;
                    txtPresentacion.Text = a.presentacion;

                    HabilitarBotones(false);
                    nuevo = false;
                }
                else
                {
                    MessageBox.Show("Debe seleccionar en la tabla a quien desea editar", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repo.Leer().Count == 0)
            {
                MessageBox.Show("No cuenta con ningun Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgProductos.SelectedItem != null)
                {
                    Clases.Productos a = dtgProductos.SelectedItem as Clases.Productos;
                    if (MessageBox.Show("Esta seguro de dar de baja a " + a.nombre, "Producto", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repo.Eliminar(a))
                        {
                            MessageBox.Show("Producto dado de baja", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("No se ha podido eliminar tu Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar a un Producto de la tabla", "Producto", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            habilitado(false);
            HabilitarBotones(true);
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            MenuPrincipal menuprincipal = new MenuPrincipal();
            menuprincipal.Show();
            this.Close();
        }
    }
}
