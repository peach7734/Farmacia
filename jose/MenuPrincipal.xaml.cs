﻿using Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace jose
{
    /// <summary>
    /// Interaction logic for MenuPrincipal.xaml
    /// </summary>
    public partial class MenuPrincipal : Window
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void btnClientes_Click(object sender, RoutedEventArgs e)
        {
            ClienteFarmacia cliente = new ClienteFarmacia();
            cliente.Show();
            this.Close();
        }

        private void btnEmpleados_Click(object sender, RoutedEventArgs e)
        {
            Empleado empleado = new Empleado();
            empleado.Show();
            this.Close();
        }

        private void btnProductos_Click(object sender, RoutedEventArgs e)
        {
            Producto producto = new Producto();
            producto.Show();
            this.Close();
        }

        private void btnCategoria_Click(object sender, RoutedEventArgs e)
        {
            Ventas g = new Ventas();
            g.Show();
            this.Close();
        }
    }
}
